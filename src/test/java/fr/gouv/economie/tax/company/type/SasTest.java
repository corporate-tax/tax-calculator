package fr.gouv.economie.tax.company.type;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class SasTest {

    @Test
    void calculateTax() {
        Sas company = new Sas("TestCompany", "72226839659632", "96 Place du Jeu de Paume, VIERZON, 18100, France");

        BigDecimal calculatedTax = company.calculateTax(new BigDecimal("100"));

        assertThat(calculatedTax).isEqualTo(new BigDecimal("33"));
    }
}