package fr.gouv.economie.tax.company.type;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class AutoEntrepriseTest {

    @Test
    void calculateTax() {
        AutoEntreprise company = new AutoEntreprise("TestCompany", "72226839659632");

        BigDecimal calculatedTax = company.calculateTax(new BigDecimal("245"));

        assertThat(calculatedTax).isEqualTo(new BigDecimal("62"));
    }
}