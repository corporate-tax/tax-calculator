package fr.gouv.economie.tax.calculator;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class TaxCalculatorTest {

    @Test
    void calculate() {
        TaxCalculator taxCalculator = new TaxCalculator() {
            @Override
            protected BigDecimal calculateInternal(BigDecimal annualSalesRevenues) {
                return annualSalesRevenues.subtract(BigDecimal.ONE);
            }
        };

        assertThat(taxCalculator.calculate(new BigDecimal("10.02"))).isEqualTo(new BigDecimal("10"));
    }
}
