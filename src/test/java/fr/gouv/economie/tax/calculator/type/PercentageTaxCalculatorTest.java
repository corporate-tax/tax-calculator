package fr.gouv.economie.tax.calculator.type;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class PercentageTaxCalculatorTest {

    @Test
    void calculateInternal() {
        PercentageTaxCalculator percentageTaxCalculator = new PercentageTaxCalculator(BigDecimal.TEN);

        BigDecimal annualSalesRevenues = new BigDecimal("120");
        BigDecimal calculatedTax = percentageTaxCalculator.calculateInternal(annualSalesRevenues);

        assertThat(calculatedTax).isEqualTo(new BigDecimal("12.00"));
    }

}