package fr.gouv.economie.tax.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class TaxCalculator {

    protected abstract BigDecimal calculateInternal(BigDecimal annualSalesRevenues);

    public BigDecimal calculate(BigDecimal annualSalesRevenues) {
        return calculateInternal(annualSalesRevenues).setScale(0, RoundingMode.CEILING);
    }

}
