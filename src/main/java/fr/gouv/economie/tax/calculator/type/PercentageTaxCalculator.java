package fr.gouv.economie.tax.calculator.type;

import fr.gouv.economie.tax.calculator.TaxCalculator;
import lombok.AllArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.math.RoundingMode;

@ToString
@AllArgsConstructor
public class PercentageTaxCalculator extends TaxCalculator {

    public static final BigDecimal ONE_HUNDRED = new BigDecimal("100");

    private BigDecimal taxRate;

    @Override
    protected BigDecimal calculateInternal(BigDecimal annualSalesRevenues) {
        return annualSalesRevenues.multiply(taxRate).divide(ONE_HUNDRED, 2, RoundingMode.CEILING);
    }
}
