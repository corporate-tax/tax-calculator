package fr.gouv.economie.tax.company.type;

import fr.gouv.economie.tax.calculator.type.PercentageTaxCalculator;
import fr.gouv.economie.tax.company.Company;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class AutoEntreprise extends Company {

    private static final PercentageTaxCalculator TAX_CALCULATOR = new PercentageTaxCalculator(new BigDecimal("25"));

    public AutoEntreprise(String name, String siret) {
        super(name, siret, TAX_CALCULATOR);
    }
}
