package fr.gouv.economie.tax.company.type;

import fr.gouv.economie.tax.calculator.type.PercentageTaxCalculator;
import fr.gouv.economie.tax.company.Company;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class Sas extends Company {

    private static final PercentageTaxCalculator TAX_CALCULATOR = new PercentageTaxCalculator(new BigDecimal("33"));

    private final String address;

    public Sas(String name, String siret, String address) {
        super(name, siret, TAX_CALCULATOR);
        this.address = address;
    }
}
