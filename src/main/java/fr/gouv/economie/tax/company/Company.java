package fr.gouv.economie.tax.company;

import fr.gouv.economie.tax.calculator.TaxCalculator;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class Company {

    private final String name;
    private final String siret;
    private final TaxCalculator taxCalculator;

    public BigDecimal calculateTax(BigDecimal annualSalesRevenues) {
        return taxCalculator.calculate(annualSalesRevenues);
    }

}
