package fr.gouv.economie.tax;

import fr.gouv.economie.tax.company.Company;
import fr.gouv.economie.tax.company.type.AutoEntreprise;
import fr.gouv.economie.tax.company.type.Sas;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Map;

@Slf4j
public class Application {

    public static void main(String[] args) {
        // Auto-entreprise
        Company autoEntreprise = new AutoEntreprise("Rippin and Sons", "51264978889814");
        BigDecimal autoEntrepriseAsr = new BigDecimal("42131.33");
        // SAS
        Company sas = new Sas("Ruecker LLC", "28885667553020", "96 Place du Jeu de Paume, VIERZON, 18100, France");
        BigDecimal sasAsr = new BigDecimal("5998555989");

        // Display
        Map.of(autoEntreprise, autoEntrepriseAsr, sas, sasAsr).forEach((company, annualSalesRevenue) ->
                log.info(String.format("[%s] %s - %s : %s -> %s EUR (%s)",
                        company.getClass().getSimpleName(),
                        company.getName(),
                        company.getSiret(),
                        annualSalesRevenue,
                        company.calculateTax(annualSalesRevenue),
                        company.getTaxCalculator())));
    }

}
